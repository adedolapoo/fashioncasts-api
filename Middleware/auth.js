var passport = require('passport')
const { User, AuthToken } = require('../Modules/User/models');

module.exports = {

    isValidUser: function(){
        if (passport.authenticate('jwt', { session: false })) {
            const token = req.cookies.auth_token || req.headers.authorization;
            if (token) {                
                // look for an auth token that matches the cookie or header
                const authToken = AuthToken.find({ where: { token }, include: User });

                if (authToken) {
                    req.user = authToken.User;
                }
            }
            next();
        } else return res.status(401).json({ 'msg': 'UnAuthorized Request!' })
    },

    isValidAdmin: function(req, res, next) {
        
    }
}