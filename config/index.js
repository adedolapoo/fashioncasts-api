module.exports = {
    app:{
        port: process.env.PORT
    },
    db: {
        url: process.env.DEV_URL,
        host: process.env.DEV_DB_HOST,
        port: parseInt(process.env.DEV_DB_PORT) || 27017,
        name: process.env.DEV_DB_DATABASE || ''
    },
    data: {
        limit: '50mb',
        extended: false
    },
    mail: {
        auth: {
            api_user: process.env.SENDGRID_USERNAME,
            api_key: process.env.SENDGRID_PASSWORD
        },    
    },
    sms: {
        africastalking: {
            apiKey: 'f39adb22724c3c6686c19dce339dcb3e5344bb412512b294193c27139f5a0b93', //         // use your sandbox app API key for development in the test environment
            username: 'weserve',      // use 'sandbox' for development in the test environment
        },
        twilio:{
            apiKey: '',
            username: ''
        }
    },
    video:{
        vimeo:{
            clientId: "b4c2351fdc0293b73e0edbf5db27685aa52af6d9",
            clientSecret: "jwSfO3NBVqNY+VNbA9D8u+biO460z0pVl8DmlT/e8matjD+UeyVLHNzsAXObVfCav1uFds8rPR3rvu+DXRL0kWRwEk8R09phcQ64qThLMLLMrXzHoH/Q774f2McIQn2f",
            accessToken: "a852ca944c59ab717ee9a94ed3c6b50a"
        },
    }
}